/*
 *
 *      Copyright (c) 2018-2099, lipengjun All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the fly2you.cn developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lipengjun (939961241@qq.com)
 *
 */
package com.platform.modules.wx.controller;

import com.platform.common.utils.RestResponse;
import com.platform.modules.wx.form.WxUserBatchTaggingForm;
import com.platform.modules.wx.form.WxUserTagForm;
import com.platform.modules.wx.service.WxUserTagsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.tag.WxUserTag;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 公众号用户标签
 *
 * @author 李鹏军
 */
@RestController
@RequestMapping("/manage/wxUserTags")
@Api(tags = {"公众号用户标签-管理后台"})
public class WxUserTagsManageController {
    @Autowired
    private WxUserTagsService wxUserTagsService;

    /**
     * 查询用户标签
     */
    @GetMapping("/list")
    @RequiresPermissions("wx:wxuser:info")
    @ApiOperation(value = "列表")
    public RestResponse list() throws WxErrorException {
        List<WxUserTag> wxUserTags = wxUserTagsService.getWxTags();
        return RestResponse.success().put("list", wxUserTags);
    }

    /**
     * 修改或新增标签
     */
    @PostMapping("/save")
    @RequiresPermissions("wx:wxuser:save")
    @ApiOperation(value = "保存")
    public RestResponse save(@RequestBody WxUserTagForm form) throws WxErrorException {
        Long tagid = form.getId();
        if (tagid == null || tagid <= 0) {
            wxUserTagsService.creatTag(form.getName());
        } else {
            wxUserTagsService.updateTag(tagid, form.getName());
        }
        return RestResponse.success();
    }

    /**
     * 删除标签
     */
    @PostMapping("/delete/{tagid}")
    @RequiresPermissions("wx:wxuser:save")
    @ApiOperation(value = "删除标签")
    public RestResponse delete(@PathVariable("tagid") Long tagid) throws WxErrorException {
        if (tagid == null || tagid <= 0) {
            return RestResponse.error("标签ID不得为空");
        }
        wxUserTagsService.deleteTag(tagid);
        return RestResponse.success();
    }

    /**
     * 批量给用户打标签
     */
    @PostMapping("/batchTagging")
    @RequiresPermissions("wx:wxuser:save")
    @ApiOperation(value = "批量给用户打标签")
    public RestResponse batchTagging(@RequestBody WxUserBatchTaggingForm form) throws WxErrorException {

        wxUserTagsService.batchTagging(form.getTagid(), form.getOpenidList());
        return RestResponse.success();
    }

    /**
     * 批量移除用户标签
     */
    @PostMapping("/batchUnTagging")
    @RequiresPermissions("wx:wxuser:save")
    @ApiOperation(value = "批量移除用户标签")
    public RestResponse batchUnTagging(@RequestBody WxUserBatchTaggingForm form) throws WxErrorException {
        wxUserTagsService.batchUnTagging(form.getTagid(), form.getOpenidList());
        return RestResponse.success();
    }
}
