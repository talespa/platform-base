package com.platform.modules.wx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.platform.common.utils.RestResponse;
import com.platform.modules.wx.entity.TemplateMsgLogEntity;
import com.platform.modules.wx.service.TemplateMsgLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;

/**
 * 模版消息发送记录
 *
 * @author 李鹏军
 */
@RestController
@RequestMapping("/manage/templateMsgLog")
@Api(tags = {"模板消息发送记录-管理后台"})
public class TemplateMsgLogManageController {
    @Autowired
    private TemplateMsgLogService templateMsgLogService;

    /**
     * 列表
     */
    @GetMapping("/list")
    @RequiresPermissions("wx:templatemsglog:list")
    @ApiOperation(value = "列表")
    public RestResponse list(@RequestParam Map<String, Object> params) {
        IPage page = templateMsgLogService.queryPage(params);

        return RestResponse.success().put("page", page);
    }

    /**
     * 信息
     */
    @GetMapping("/info/{logId}")
    @RequiresPermissions("wx:templatemsglog:info")
    @ApiOperation(value = "详情")
    public RestResponse info(@PathVariable("logId") String logId) {
        TemplateMsgLogEntity templateMsgLog = templateMsgLogService.getById(logId);

        return RestResponse.success().put("templateMsgLog", templateMsgLog);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    @RequiresPermissions("wx:templatemsglog:save")
    @ApiOperation(value = "保存")
    public RestResponse save(@RequestBody TemplateMsgLogEntity templateMsgLog) {
        templateMsgLogService.save(templateMsgLog);

        return RestResponse.success();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @RequiresPermissions("wx:templatemsglog:update")
    @ApiOperation(value = "修改")
    public RestResponse update(@RequestBody TemplateMsgLogEntity templateMsgLog) {
        templateMsgLogService.updateById(templateMsgLog);
        return RestResponse.success();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @RequiresPermissions("wx:templatemsglog:delete")
    @ApiOperation(value = "删除")
    public RestResponse delete(@RequestBody String[] logIds) {
        templateMsgLogService.removeByIds(Arrays.asList(logIds));
        return RestResponse.success();
    }
}
